#include <ODrive.hpp>

ODrive::ODrive(UART_HandleTypeDef uart)
	: huart6(uart) {

	currentState[0] = AXIS_STATE_IDLE;
	currentState[1] = AXIS_STATE_IDLE;
	currentVelocity[0] = 0;
	currentVelocity[1] = 0;
	/* Send a dummy line ending to make sure the ODrive will accept
	 * messages that follow!
	 */
	char dummy[2] = "\n";
	HAL_UART_Transmit(&huart6, (uint8_t*)dummy, sizeof(dummy)-1, 1000);
	for(int i=0;i<9000;i++);
}

void ODrive::set_velocity(Axis_t axis, int velocity){
	std::ostringstream vel;

	//if(currentState[axis] == AXIS_STATE_IDLE && velocity)
	//  set_state(axis, AXIS_STATE_SENSORLESS_CONTROL, true);
	if(axis == AXIS_1)
		velocity = -velocity;

	if(currentVelocity[axis] != velocity){
		vel << "v " << axis << " " << velocity << " 0\n";
		sendString(&vel);
	}

	currentVelocity[axis] = velocity;

	//if(!velocity && currentState[axis] == AXIS_STATE_SENSORLESS_CONTROL)
	//  set_state(axis, AXIS_STATE_IDLE, true);
}

int ODrive::get_velocity(Axis_t axis){
	return currentVelocity[axis];
}

bool ODrive::set_state(Axis_t axis, AxisState_t state, bool wait){
	int timeout_ctr = 100;
	std::ostringstream reqState;
	currentState[axis] = state;

	reqState << "w axis" << axis << ".requested_state " << state << "\n";
	sendString(&reqState);
#if 1
    if (wait) {
        do {
        	HAL_Delay(100);
            reqState.str("");
            reqState << "r axis" << axis << ".current_state\n";
            sendString(&reqState);
        } while (std::atoi(readString().c_str()) != state && --timeout_ctr > 0);
    }
#endif
    return timeout_ctr > 0;
}

void ODrive::sendString(std::ostringstream *str){
	int n = str->str().length();
	char cmd[n + 1];

	strcpy(cmd, str->str().c_str());
	HAL_UART_Transmit(&huart6, (uint8_t*)cmd, n, 1000);
}

std::string ODrive::readString() {
    std::string str = "";
    static const unsigned long timeout = 1000;
    unsigned long timeout_start = HAL_GetTick();
    for (;;) {
        char c;
        HAL_UART_Receive(&huart6, (uint8_t*)&c, 1, 1000);
        if (c == '\n')
            break;
        else if(HAL_GetTick() - timeout_start >= timeout)
        	break;
        str.append(&c);
    }
    return str;
}
