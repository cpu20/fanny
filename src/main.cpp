/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include <VL53L0X.hpp>
#include <ODrive.hpp>
#include <stdio.h>
#include "main.h"
#include "i2c.h"
#include "gpio.h"
#include "dma.h"
#include "adc.h"
#include "usart.h"
#include "stm32f7xx.h"

volatile uint16_t range[ToF_SENSORS];
uint32_t adc_dma_buf[2] = { 0 };

/* Variable to keep track of the program status. */
volatile ProgramState_t loopState = Program_state_idle;
volatile RobotState_t robotState = Robot_idle;
volatile uint32_t sensorHistory[ToF_SENSORS] = {0};
volatile uint8_t sensorsInRange = 0;
volatile uint8_t sensorNumber = 0;

void SystemClock_Config(void);

int main(void)
{
	VL53L0X *tofSensors[ToF_SENSORS];
	/* Initialisation of the ST HAL library. */
	HAL_Init();

	/* Low level drivers init. */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_USART3_UART_Init();
	MX_USART6_UART_Init();
	MX_I2C1_Init();
	MX_ADC1_Init();

	/* Initialise the ODrive object. */
	ODrive odrv(huart6);

	/* Start the ADC conversion for the line sensors. */
	HAL_ADC_Start_DMA(&hadc1, adc_dma_buf, 2);

    /* Create two objects for our sensors. */
#if ToF_SENSORS > 0
    tofSensors[0] = new VL53L0X(&hi2c1, ToF4_GPIO_Port, ToF4_Pin, false);
#endif
#if ToF_SENSORS > 1
    tofSensors[1] = new VL53L0X(&hi2c1, ToF2_GPIO_Port, ToF2_Pin, false);
#endif
#if ToF_SENSORS > 2
    tofSensors[2] = new VL53L0X(&hi2c1, ToF6_GPIO_Port, ToF6_Pin, false);
#endif
#if ToF_SENSORS > 3
    tofSensors[3] = new VL53L0X(&hi2c1, ToF7_GPIO_Port, ToF7_Pin, false);
#endif
#if ToF_SENSORS > 4
    tofSensors[4] = new VL53L0X(&hi2c1, ToF5_GPIO_Port, ToF5_Pin, false);
#endif
#if ToF_SENSORS > 5
    tofSensors[5] = new VL53L0X(&hi2c1, ToF6_GPIO_Port, ToF6_Pin, false);
#endif
#if ToF_SENSORS > 6
    tofSensors[6] = new VL53L0X(&hi2c1, ToF7_GPIO_Port, ToF7_Pin, false);
#endif

	for(int i=0;i<ToF_SENSORS;i++){
	  /* Disable standby mode and change the I2C address. */
	  tofSensors[i]->hwStandby(false);
	  tofSensors[i]->setAddress((0x12 + i)<<1);
	  /* Initialise the sensor. */
	  tofSensors[i]->init(false);
	  /* Fastest timing budget is 20ms. */
	  tofSensors[i]->setMeasurementTimingBudget(40000); // In us
	  tofSensors[i]->setTimeout(500);
	  /* Already start the continuous measurements as fast as possible. */
	  tofSensors[i]->startContinuous();
	}

	uint8_t buttonState = 0, prevButtonState = 1;
	/* Just wait for the button press to begin. */
	while(true){
	  buttonState = HAL_GPIO_ReadPin(START_BUTTON_GPIO_Port, START_BUTTON_Pin);
	  if(buttonState != prevButtonState){
	    HAL_Delay(40);
	    if(HAL_GPIO_ReadPin(START_BUTTON_GPIO_Port, START_BUTTON_Pin) == buttonState){
	  	  prevButtonState = buttonState;
	   	  if(HAL_GPIO_ReadPin(START_BUTTON_GPIO_Port, START_BUTTON_Pin) == GPIO_PIN_RESET){
	   		break;
	   	  }
	     }
	  }
	}

	/* LED countdown. */
	for(uint8_t i=0;i<5;i++){
	  HAL_GPIO_WritePin(START_LED_GPIO_Port, START_LED_Pin, GPIO_PIN_SET);
	  HAL_Delay(700);
	  HAL_GPIO_WritePin(START_LED_GPIO_Port, START_LED_Pin, GPIO_PIN_RESET);
	  HAL_Delay(300);
	}
	loopState = Program_state_running;

	/* Put both motor channels in closed loop control. */
	odrv.set_state(ODrive::AXIS_0, ODrive::AXIS_STATE_CLOSED_LOOP_CONTROL, true);
	odrv.set_state(ODrive::AXIS_1, ODrive::AXIS_STATE_CLOSED_LOOP_CONTROL, true);

	while(1){
		for(uint8_t i=0;i<ToF_SENSORS;i++){
			range[i] = tofSensors[i]->readRangeContinuousMillimeters();
#if 0
			std::ostringstream str;
			str << "Sensor[" << (int)i << "]: " << range[i] << "\r\n";
			int n = str.str().length();
			char cmd[n + 1];

			strcpy(cmd, str.str().c_str());
			HAL_UART_Transmit(&huart3, (uint8_t*)cmd, n, 1000);
#endif
			if(range[i] <= ToF_DETECTION_RANGE){
			  sensorHistory[i] = HAL_GetTick();
			  sensorsInRange |= (1 << i);
			} else{
			  sensorsInRange &= ~(1 << i);
			}
		}
#if 0
		HAL_Delay(500);
#endif
		/* Only execute the main loop when the button was pressed. */
		if(loopState == Program_state_running){
		  if(robotState == Robot_idle){
			  odrv.set_velocity(ODrive::AXIS_0, SEARCH_VELOCITY);
			  odrv.set_velocity(ODrive::AXIS_1, SEARCH_VELOCITY);
			  robotState = Robot_searching;
		  } else if(robotState == Robot_searching && sensorsInRange){
			  /* Search which sensor detected something most recently. */
			  SensorData lastDetection = {0, sensorHistory[0]};
			  uint8_t i = 0;
			  while(sensorsInRange){
				if((sensorsInRange & 0x1) && (sensorHistory[i] > lastDetection.tickValue)){
				  lastDetection.sensorNumber = i+1;
				  lastDetection.tickValue = sensorHistory[i];
				}
				sensorsInRange >>= 1;
				i++;
			  }
			  /* It may not be too long ago something was detected. */
			  if((HAL_GetTick() - lastDetection.tickValue) < ToF_TICK_THRESHOLD){
				  /* The middle sensor sees something very close. */
				  switch(lastDetection.sensorNumber){
				    case 4:
				      odrv.set_velocity(ODrive::AXIS_0, ATTACK_VELOCITY);
					  odrv.set_velocity(ODrive::AXIS_1, ATTACK_VELOCITY);
					  robotState = Robot_attacking;
					  break;
				    case 1:
				      odrv.set_velocity(ODrive::AXIS_0, 400);
				      odrv.set_velocity(ODrive::AXIS_1, 0);
				      break;
				    case 5:
				      odrv.set_velocity(ODrive::AXIS_0, 0);
				      odrv.set_velocity(ODrive::AXIS_1, 400);
				      break;
				    case 2:
				      odrv.set_velocity(ODrive::AXIS_0, 800);
				      odrv.set_velocity(ODrive::AXIS_1, 600);
				      break;
				    case 3:
				      odrv.set_velocity(ODrive::AXIS_0, 600);
				      odrv.set_velocity(ODrive::AXIS_1, 800);
				      break;
				  }
			  } else{
				robotState = Robot_searching;
				odrv.set_velocity(ODrive::AXIS_0, SEARCH_VELOCITY);
				odrv.set_velocity(ODrive::AXIS_1, SEARCH_VELOCITY);
			  }
		  } else if(robotState == Robot_attacking){
			  if(sensorsInRange & (1 << 6)){
				  odrv.set_velocity(ODrive::AXIS_0, ATTACK_VELOCITY + 200);
			  } if(sensorsInRange & (1 << 3)){
				  odrv.set_velocity(ODrive::AXIS_1, ATTACK_VELOCITY + 200);
			  } if(!(sensorsInRange & ((1 << 6) | (1 << 3) | (1 << 7)))){
				  odrv.set_velocity(ODrive::AXIS_1, SEARCH_VELOCITY);
				  odrv.set_velocity(ODrive::AXIS_1, SEARCH_VELOCITY);
				  robotState = Robot_searching;
			  }
		  } else if(robotState == Robot_lineDetected){
			  if(sensorNumber == 0){
				odrv.set_velocity(ODrive::AXIS_0, 0);
				odrv.set_velocity(ODrive::AXIS_1, 0);
				HAL_Delay(20);
				odrv.set_velocity(ODrive::AXIS_0, -400);
				HAL_Delay(500);
			  } else if(sensorNumber == 1){
				odrv.set_velocity(ODrive::AXIS_0, 0);
				odrv.set_velocity(ODrive::AXIS_1, 0);
				HAL_Delay(20);
				odrv.set_velocity(ODrive::AXIS_0, -400);
				HAL_Delay(500);
			  }
			  robotState = Robot_searching;
			  odrv.set_velocity(ODrive::AXIS_0, SEARCH_VELOCITY);
			  odrv.set_velocity(ODrive::AXIS_1, SEARCH_VELOCITY);
		  }
		} else if(loopState == Program_state_idle){
		  odrv.set_velocity(ODrive::AXIS_0, 0);
		  odrv.set_velocity(ODrive::AXIS_1, 0);
		  robotState = Robot_idle;
		}
	}
}

/**
  * @brief  Conversion complete callback in non blocking mode
  * @param  AdcHandle : AdcHandle handle
  * @retval None
  */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle)
{
  static uint32_t blackValue[2] = { 0 };

  if(!blackValue[0] || !blackValue[1]){
	  blackValue[0] = adc_dma_buf[0];
	  blackValue[1] = adc_dma_buf[1];
  } else{
	  if(((int)blackValue[0] - (int)adc_dma_buf[0]) >= 600){
		sensorNumber = 0;
		robotState = Robot_lineDetected;
	  } else if(((int)blackValue[1] - (int)adc_dma_buf[1]) >= 600){
		sensorNumber = 1;
		robotState = Robot_lineDetected;
	  }
  }
}

/**
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_USART6
                              |RCC_PERIPHCLK_I2C1;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
  PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{

}
