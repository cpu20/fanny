/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
enum ProgramState_t {
	Program_state_idle,	/* Program is waiting for a start signal. */
	Program_state_countdown, /* 5s count down until start of main loop. */
	Program_state_running, /* Main loop execution. */
	Program_state_lineDetected /* A line was detected! */
};

enum RobotState_t {
	Robot_idle,
	Robot_searching,
	Robot_attacking,
	Robot_lineDetected
};

struct SensorData{
	uint8_t sensorNumber;
	uint32_t tickValue;
};
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOH
#define GroundSens1_Pin GPIO_PIN_2
#define GroundSens1_GPIO_Port GPIOA
#define GroundSens2_Pin GPIO_PIN_3
#define GroundSens2_GPIO_Port GPIOA
#define ToF6_Pin GPIO_PIN_4
#define ToF6_GPIO_Port GPIOC
#define LD1_Pin GPIO_PIN_0
#define LD1_GPIO_Port GPIOB
#define ToF4_Pin GPIO_PIN_1
#define ToF4_GPIO_Port GPIOB
#define ToF3_Int_Pin GPIO_PIN_2
#define ToF3_Int_GPIO_Port GPIOB
#define ROT2_Pin GPIO_PIN_12
#define ROT2_GPIO_Port GPIOF
#define START_BUTTON_Pin GPIO_PIN_13
#define START_BUTTON_GPIO_Port GPIOF
#define TFT_RS_Pin GPIO_PIN_10
#define TFT_RS_GPIO_Port GPIOE
#define TFT_SCK_Pin GPIO_PIN_12
#define TFT_SCK_GPIO_Port GPIOE
#define TFT_SDI_Pin GPIO_PIN_14
#define TFT_SDI_GPIO_Port GPIOE
#define TFT_CS_Pin GPIO_PIN_15
#define TFT_CS_GPIO_Port GPIOE
#define ToF5_Int_Pin GPIO_PIN_10
#define ToF5_Int_GPIO_Port GPIOB
#define ToF1_Pin GPIO_PIN_11
#define ToF1_GPIO_Port GPIOB
#define ToF1_Int_Pin GPIO_PIN_12
#define ToF1_Int_GPIO_Port GPIOB
#define ToF6_Int_Pin GPIO_PIN_13
#define ToF6_Int_GPIO_Port GPIOB
#define LD3_Pin GPIO_PIN_14
#define LD3_GPIO_Port GPIOB
#define ToF4_Int_Pin GPIO_PIN_15
#define ToF4_Int_GPIO_Port GPIOB
#define STLK_RX_Pin GPIO_PIN_8
#define STLK_RX_GPIO_Port GPIOD
#define STLK_TX_Pin GPIO_PIN_9
#define STLK_TX_GPIO_Port GPIOD
#define START_LED_Pin GPIO_PIN_10
#define START_LED_GPIO_Port GPIOD
#define TFT_RST_Pin GPIO_PIN_11
#define TFT_RST_GPIO_Port GPIOD
#define ToF2_Pin GPIO_PIN_5
#define ToF2_GPIO_Port GPIOG
#define ToF2_Int_Pin GPIO_PIN_6
#define ToF2_Int_GPIO_Port GPIOG
#define ToF5_Pin GPIO_PIN_8
#define ToF5_GPIO_Port GPIOA
#define ToF3_Pin GPIO_PIN_9
#define ToF3_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define ROT1_Pin GPIO_PIN_14
#define ROT1_GPIO_Port GPIOG
#define SW0_Pin GPIO_PIN_3
#define SW0_GPIO_Port GPIOB
#define ToF7_Int_Pin GPIO_PIN_4
#define ToF7_Int_GPIO_Port GPIOB
#define ToF7_Pin GPIO_PIN_5
#define ToF7_GPIO_Port GPIOB
#define LD2_Pin GPIO_PIN_7
#define LD2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define ToF_SENSORS 5
#define ToF_DETECTION_RANGE 100
#define ToF_TICK_THRESHOLD 1000
#define SEARCH_VELOCITY 400
#define ATTACK_VELOCITY 1000
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
