#ifndef __ODRIVE_HPP
#define __ODRIVE_HPP

#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_conf.h"
#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>

class ODrive{
public:
    enum AxisState_t {
        AXIS_STATE_UNDEFINED = 0,           //<! will fall through to idle
        AXIS_STATE_IDLE = 1,                //<! disable PWM and do nothing
        AXIS_STATE_STARTUP_SEQUENCE = 2, //<! the actual sequence is defined by the config.startup_... flags
        AXIS_STATE_FULL_CALIBRATION_SEQUENCE = 3,   //<! run all calibration procedures, then idle
        AXIS_STATE_MOTOR_CALIBRATION = 4,   //<! run motor calibration
        AXIS_STATE_SENSORLESS_CONTROL = 5,  //<! run sensorless control
        AXIS_STATE_ENCODER_INDEX_SEARCH = 6, //<! run encoder index search
        AXIS_STATE_ENCODER_OFFSET_CALIBRATION = 7, //<! run encoder offset calibration
        AXIS_STATE_CLOSED_LOOP_CONTROL = 8  //<! run closed loop control
    };

    enum Axis_t {
    	AXIS_0 = 0,
		AXIS_1 = 1
    };

    UART_HandleTypeDef huart6;

    ODrive(UART_HandleTypeDef uart);
    void set_velocity(Axis_t axis, int velocity);
    int get_velocity(Axis_t axis);
    bool set_state(Axis_t axis, AxisState_t state, bool wait);
private:
    AxisState_t currentState[2];
    int currentVelocity[2];

    void sendString(std::ostringstream *str);
    std::string readString();
};

#endif
